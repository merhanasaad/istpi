jQuery(function($) {

    var abc = '<div class="switcherWrap"><h4> Style Slector <a href="#"> <i class="fa fa-cog fa-spin fa-fw"></i> </a></h4><div class="section-switch"><ul class="nav nav-tabs" role="tablist"> <li role="presentation" class="active"><a href="#color-variation" aria-controls="color-variation" role="tab" data-toggle="tab">Choose color</a></li>  <li role="presentation"><a href="#pages-variation" aria-controls="pages-variation" role="tab" data-toggle="tab">Email support</a></li></ul><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="color-variation"><div class="color-listed"><span class="default theme-active" data-color="style"> <a href="#" class="color">1 </a></span><span class="indigo" data-color="indigo"> <a href="#" class="color">1 </a></span> <span class="green" data-color="green"> <a href="#" class="color">1 </a></span> <span class="yello" data-color="yello"> <a href="#" class="color">1 </a></span><span class="red" data-color="red"> <a href="#" class="color">1 </a></span> </div></div><div role="tabpanel" class="tab-pane" id="pages-variation"><div class="pagelink"><h5> supazwebsolution@gmail.com </h5></div></div></div></div></div>';

    $("body").append(abc);

    $('.switcherWrap h4 a').on("click", function(e) {
        e.preventDefault();

        if ($('.switcherWrap').hasClass("active")) {
            $('.switcherWrap').animate({
                left: "0px"
            }, 300).removeClass("active");

        } else {
            $('.switcherWrap').animate({
                left: "-270px"
            }, 300).addClass("active");
        }

    });

    // theme change js

    var panel = jQuery('.switcherWrap');
    jQuery('span', panel).on("click", function(e) {
        e.preventDefault();
        var color = jQuery(this).attr("data-color");
        // var data_logo = jQuery(this).attr("data-logo");
        setColor(color);
        jQuery('span', panel).removeClass("theme-active");
        jQuery(this).addClass("theme-active");
    });

    var setColor = function(color) {
        jQuery('#option_color').attr("href", "css/" + color + ".css");

    };
})
